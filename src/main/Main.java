package main;
import exceptions.AddGunException;
import io.XMLShip;
import model.*;

import io.*;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws JAXBException {
        Scanner in = new Scanner(System.in);

        Flotilla flotilla = new Flotilla();

        int flag;
        while (true){
            System.out.println("1 -> add destroyer");
            System.out.println("2 -> add cruiser");

            System.out.println("3 -> print flotilla");

            System.out.println("0 -> exit");

            flag = in.nextInt();

            switch (flag){
                case 1: {
                    Destroyer d = new Destroyer();
                    flotilla.getFlotilla().add(d);
                    break;
                }
                case 2:{
                    Cruiser c = new Cruiser();
                    flotilla.getFlotilla().add(c);
                    break;
                }
                case 3:{
                    for (Ship s:flotilla.getFlotilla()){
                        System.out.println(s.toString());
                    }
                    break;
                }

                case 4:{
                    XMLShip xmls = new XMLShip();
                    xmls.serialize(flotilla,"Armada.xml");
                    break;
                }
                case 78:{
                    try {
                        flotilla.getFlotilla().getFirst().addGuns(new Gun(130,8,8000,6));
                    } catch (AddGunException e) {
                        e.printStackTrace();
                    }
                    break;
                }
                case 0:{
                    return;
                }
            }

        }

    }
}
