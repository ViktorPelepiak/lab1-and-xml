package io;

import model.Flotilla;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.IOException;


public interface IO<T> {

    void serialize(Flotilla flotilla, String path) throws IOException, JAXBException;
    Flotilla deserialize(String path) throws IOException, JAXBException;

}