package io;


        import model.Flotilla;

        import javax.xml.bind.JAXBContext;
        import javax.xml.bind.JAXBException;
        import javax.xml.bind.Marshaller;
        import javax.xml.bind.Unmarshaller;
        import java.io.File;

public class XMLShip implements IO{
    @Override
    public void serialize(Flotilla flotilla, String path) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Flotilla.class);
        Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        jaxbMarshaller.marshal(flotilla, new File(path));
    }

    @Override
    public Flotilla deserialize(String path) throws JAXBException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Flotilla.class);
        Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        Flotilla flotilla = (Flotilla) jaxbUnmarshaller.unmarshal(new File(path));
        return flotilla;
    }
}
