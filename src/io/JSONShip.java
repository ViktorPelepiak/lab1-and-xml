package io;

import com.sun.xml.internal.ws.developer.SerializationFeature;
import model.Flotilla;
import model.Ship;

import javax.xml.bind.JAXBException;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;


public class JSONShip implements IO{

    @Override
    public void serialize(Flotilla flotilla, String path) throws IOException {
        File file = new File(path);
        ObjectMapper objMapper = new ObjectMapper();
        objMapper.configure(SerializationFeature.INDENT_OUTPUT, true);
        StringWriter stringWriter = new StringWriter();
        objMapper.writeValue(stringWriter, flotilla);
        FileWriter fw = new FileWriter(file);
        fw.write(stringWriter.toString());
        fw.close();
    }

    @Override
    public Flotilla deserialize(String path) throws IOException, JAXBException{

        Flotilla f=new Flotilla();

        return f;
    }
}
