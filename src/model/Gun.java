package model;

import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
public class Gun{
    private int calibre;
    private int quantity;

    private double distance;

    private double rateOfFire;

    private boolean mainCalibre;
    private boolean auxiliaryCalibre;
    private boolean airDefence;

    public Gun() {
        this.setCalibre(0);
        this.setQuantity(0);
        this.distance = 0;
        this.rateOfFire = 0;
    }

    public Gun(int calibre, int quantity, double distance, double rateOfRire){
        this.setCalibre(calibre);
        this.setQuantity(quantity);
        this.distance = distance;
        this.rateOfFire = rateOfRire;
    }


    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getRateOfFire() {
        return rateOfFire;
    }

    public void setRateOfFire(double rateOfFire) {
        this.rateOfFire = rateOfFire;
    }

    public boolean isMainCalibre() {
        return mainCalibre;
    }

    public void setMainCalibre(boolean mainCalibre) {
        this.mainCalibre = mainCalibre;
    }

    public boolean isAuxiliaryCalibre() {
        return auxiliaryCalibre;
    }

    public void setAuxiliaryCalibre(boolean auxiliaryCalibre) {
        this.auxiliaryCalibre = auxiliaryCalibre;
    }

    public boolean isAirDefence() {
        return airDefence;
    }

    public void setAirDefence(boolean airDefence) {
        this.airDefence = airDefence;
    }

    public int getCalibre() {
        return calibre;
    }

    public void setCalibre(int calibre) {
        this.calibre = calibre;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
