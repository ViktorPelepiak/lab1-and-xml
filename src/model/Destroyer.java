package model;

import exceptions.AddGunException;

public class Destroyer extends Ship{
    private Torpedo torpedoes;
    private static final int maximalCalibre = 130;
    private static final int minimalCalibre = 76;

    public Destroyer(String name,Country country, double displacement, double length, double width, double powerOfEngine, double maximumSpeed, double frontArmor, double sideArmor, double aftArmor, Torpedo torpedoes){
        this.setName(name);
        this.setCountry(country);
        this.setDisplacement(displacement);
        this.setLength(length);
        this.setWidth(width);
        this.setPowerOfEngine(powerOfEngine);
        this.setMaximumSpeed(maximumSpeed);
        this.setFrontArmor(frontArmor);
        this.setSideArmor(sideArmor);
        this.setAftArmor(aftArmor);
        this.torpedoes = torpedoes;
        this.setMaxMainCalibreQuantity(6);
        this.setMaxAuxiliaryCalibreQuantity(6);
        this.setMainCalibreQuantity(0);
        this.setAuxiliaryCalibreQuantity(0);
    }
    public Destroyer(){
        this.setName("NONE");
        this.setCountry(Country.NONE);
        this.setDisplacement(0);
        this.setLength(0);
        this.setWidth(0);
        this.setPowerOfEngine(0);
        this.setMaximumSpeed(0);
        this.setFrontArmor(0);
        this.setSideArmor(0);
        this.setAftArmor(0);
        this.torpedoes = new Torpedo(240,10,9500,7,1);
        this.setMaxMainCalibreQuantity(10);
        this.setMaxAuxiliaryCalibreQuantity(0);
        this.setMainCalibreQuantity(0);
        this.setAuxiliaryCalibreQuantity(0);
    }

    public Torpedo getTorpedoes() {
        return torpedoes;
    }

    public void setTorpedoes(Torpedo torpedoes) {
        this.torpedoes = torpedoes;
    }

    @Override
    public void addGuns(Gun w) throws AddGunException {
        if(w.getCalibre()>maximalCalibre)throw new AddGunException("Too large a caliber for a destroyer! The caliber should not be larger than 130mm");

        w.setAirDefence(true);
        if(getGuns().size()==0){
            if(w.getCalibre()>=minimalCalibre){
                w.setMainCalibre(true);
                if(getMainCalibreQuantity()+w.getQuantity()>getMaxMainCalibreQuantity()){
                    setMainCalibreQuantity(getMaxMainCalibreQuantity());
                    w.setQuantity(getMaxMainCalibreQuantity());
                    throw new AddGunException("Exceeded the maximum number of guns of main calibre!!! The maximum number of guns of main calibre is set");
                }else{
                    setMainCalibreQuantity(w.getQuantity());
                }
                getGuns().add(w);
            }
        }else{
            if(!addExistingCalibre(w)) {
                Gun main = getMainCalibre();
                if (w.getCalibre()>main.getCalibre()){
                    changeMainCalibre(w);
                }else addNewAuxiliaryCalibre(w);
            }
        }
    }

    @Override
    public String toString() {
        String s="";
        s+="\n"+getClass().getName()+"\n";
        s+="Name: ";s += getName();s+="\n";
        s+="Country: ";s += getCountry();s+="\n";
        s+="Displacement: ";s += getDisplacement();s+="\n";
        s+="Length: ";s += getLength();s+="\n";
        s+="Width: ";s += getWidth();s+="\n";
        s+="Power of engine : ";s += getPowerOfEngine();s+="\n";
        s+="Maximal speed: ";s += getMaximumSpeed();s+="\n";
        s+="Front armor: ";s += getFrontArmor();s+="\n";
        s+="Side armor: ";s += getSideArmor();s+="\n";
        s+="Aft armor: ";s += getAftArmor();s+="\n";
        s+="Maximal main calibre quantity: ";s += getMaxMainCalibreQuantity();s+="\n";
        s+="Maximal auxiliary calibre quantity: ";s += getMaxAuxiliaryCalibreQuantity();s+="\n";
        s+="Main calibre quantity: ";s += getMainCalibreQuantity();s+="\n";
        s+="Auxiliary calibre quantity: ";s += getAuxiliaryCalibreQuantity();s+="\n";

        s+="model.Cruiser.Torpedo:\n";
        s+="Calibre: ";s += getTorpedoes().getCalibre();s+="\n";
        s+="Quantity: ";s += getTorpedoes().getQuantity();s+="\n";
        s+="Distance: ";s += getTorpedoes().getDistance();s+="\n";
        s+="Speed: ";s += getTorpedoes().getSpeed();s+="\n";
        s+="Rate of fire: ";s += getTorpedoes().getRateOfFire();s+="\n";

        s+="\nGuns:\n";
        for (Gun g:getGuns()){
            s+="\nCalibre: "+g.getCalibre()+"mm\n";
            s+="Quantity: "+g.getQuantity()+"\n";
            s+="Distance of fire: "+g.getDistance()+"km\n";
            s+="Rate of fire: "+g.getRateOfFire()+"spm\n";
            s+="Specialisation:\n";
            if(g.isMainCalibre())s+="\tMain calibre\n";
            if(g.isAuxiliaryCalibre())s+="\tAuxiliary calibre\n";
            if(g.isAirDefence())s+="\tAir defence\n";
        }

        return s;
    }
}
