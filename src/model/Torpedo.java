package model;

import javax.xml.bind.annotation.XmlRootElement;

//@XmlRootElement
public class Torpedo{
    private int calibre;
    private int quantity;

    private double distance;
    private double speed;

    private double rateOfFire;

    public Torpedo() {
        this.setCalibre(0);
        this.setQuantity(0);
        this.distance = 0;
        this.speed = 0;
        this.rateOfFire = 0;
    }

    public Torpedo(int calibre, int quantity, double distance, double speed, double rateOfRire){
        this.setCalibre(calibre);
        this.setQuantity(quantity);
        this.distance = distance;
        this.speed = speed;
        this.rateOfFire = rateOfRire;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

    public double getRateOfFire() {
        return rateOfFire;
    }

    public void setRateOfFire(double rateOfFire) {
        this.rateOfFire = rateOfFire;
    }

    public int getCalibre() {
        return calibre;
    }

    public void setCalibre(int calibre) {
        this.calibre = calibre;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}
