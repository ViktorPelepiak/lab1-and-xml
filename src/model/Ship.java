package model;

import exceptions.AddGunException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;
import java.util.LinkedList;

@XmlTransient
@XmlAccessorType(XmlAccessType.FIELD)
abstract public class Ship{
    private String name;
    private Country country;

    private double displacement;
    private double length;
    private double width;

    private double powerOfEngine;
    private double maximumSpeed;

    private LinkedList<Gun> guns = new LinkedList<>();

    private double frontArmor;
    private double sideArmor;
    private double aftArmor;

    private int maxMainCalibreQuantity;
    private int maxAuxiliaryCalibreQuantity;

    private int MainCalibreQuantity;
    private int AuxiliaryCalibreQuantity;


    public void addGuns(Gun g) throws AddGunException {
        guns.add(g);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public double getDisplacement() {
        return displacement;
    }

    public void setDisplacement(double displacement) {
        this.displacement = displacement;
    }

    public double getLength() {
        return length;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getPowerOfEngine() {
        return powerOfEngine;
    }

    public void setPowerOfEngine(double powerOfEngine) {
        this.powerOfEngine = powerOfEngine;
    }

    public double getMaximumSpeed() {
        return maximumSpeed;
    }

    public void setMaximumSpeed(double maximumSpeed) {
        this.maximumSpeed = maximumSpeed;
    }

    public LinkedList<Gun> getGuns() {
        return guns;
    }

    public void setWeapons(LinkedList<Gun> guns) {
        this.guns = guns;
    }

    public double getFrontArmor() {
        return frontArmor;
    }

    public void setFrontArmor(double frontArmor) {
        this.frontArmor = frontArmor;
    }

    public double getSideArmor() {
        return sideArmor;
    }

    public void setSideArmor(double sideArmor) {
        this.sideArmor = sideArmor;
    }

    public double getAftArmor() {
        return aftArmor;
    }

    public void setAftArmor(double aftArmor) {
        this.aftArmor = aftArmor;
    }

    public int getMaxMainCalibreQuantity() {
        return maxMainCalibreQuantity;
    }

    public int getMaxAuxiliaryCalibreQuantity() {
        return maxAuxiliaryCalibreQuantity;
    }

    public void setMaxMainCalibreQuantity(int maxMainCalibreQuantity) {
        this.maxMainCalibreQuantity = maxMainCalibreQuantity;
    }

    public void setMaxAuxiliaryCalibreQuantity(int maxAuxiliaryCalibreQuantity) {
        this.maxAuxiliaryCalibreQuantity = maxAuxiliaryCalibreQuantity;
    }

    public int getMainCalibreQuantity() {
        return MainCalibreQuantity;
    }

    public void setMainCalibreQuantity(int mainCalibreQuantity) {
        MainCalibreQuantity = mainCalibreQuantity;
    }

    public int getAuxiliaryCalibreQuantity() {
        return AuxiliaryCalibreQuantity;
    }

    public void setAuxiliaryCalibreQuantity(int auxiliaryCalibreQuantity) {
        AuxiliaryCalibreQuantity = auxiliaryCalibreQuantity;
    }

    public Gun getMainCalibre(){
        Gun main = getGuns().getFirst();
        for (Gun g:getGuns()){
            if(g.getCalibre() > main.getCalibre()){
                main = g;
            }
        }
        return main;
    }

    public boolean addExistingCalibre(Gun w) throws AddGunException {
        for (Gun g:getGuns()){
            if(g.getCalibre() == w.getCalibre()){
                if(g.isMainCalibre()){
                    if(getMainCalibreQuantity()+w.getQuantity()>getMaxMainCalibreQuantity()){
                        w.setQuantity(getMaxMainCalibreQuantity()-getMainCalibreQuantity());
                        setMainCalibreQuantity(getMaxMainCalibreQuantity());
                        throw new AddGunException("Exceeded the maximum number of guns of main calibre!!! The maximum number of guns of main calibre is set");
                    }
                }else if(g.isAuxiliaryCalibre()){
                    addAuxiliaryCalibre(w);
                }
                return true;
            }
        }
        return false;
    }

    public void addAuxiliaryCalibre(Gun w) throws AddGunException {
        if(getAuxiliaryCalibreQuantity()+w.getQuantity()>getMaxAuxiliaryCalibreQuantity()){
            setAuxiliaryCalibreQuantity(getMaxAuxiliaryCalibreQuantity());
            w.setQuantity(getMaxAuxiliaryCalibreQuantity()-getAuxiliaryCalibreQuantity());
            setAuxiliaryCalibreQuantity(getAuxiliaryCalibreQuantity()+w.getQuantity());
            throw new AddGunException("Exceeded the maximum number of guns of auxiliary calibre!!! The maximum number of guns of auxiliary calibre is set");
        }
    }

    public void addNewAuxiliaryCalibre(Gun g) throws AddGunException {
        if(getAuxiliaryCalibreQuantity()+g.getQuantity()>getMaxAuxiliaryCalibreQuantity()){
            setAuxiliaryCalibreQuantity(getMaxAuxiliaryCalibreQuantity());
            g.setQuantity(getMaxAuxiliaryCalibreQuantity()-getAuxiliaryCalibreQuantity());
            setAuxiliaryCalibreQuantity(getAuxiliaryCalibreQuantity()+g.getQuantity());
            if(g.getQuantity()==0){
                throw new AddGunException("Exceeded the maximum number of guns of auxiliary calibre!!! These guns were not installed");
            }
            getGuns().add(g);
            throw new AddGunException("Exceeded the maximum number of guns of auxiliary calibre!!! The maximum number of guns of auxiliary calibre is set");
        }
    }

    public void changeMainCalibre(Gun w) throws AddGunException {
        Gun m = getMainCalibre();
        w.setMainCalibre(true);
        m.setMainCalibre(false);
        m.setAuxiliaryCalibre(true);

        if(getMaxAuxiliaryCalibreQuantity()-getAuxiliaryCalibreQuantity()<m.getQuantity()){
            addAuxiliaryCalibre(m);
        }else{
            m.setQuantity(getMaxAuxiliaryCalibreQuantity()-getAuxiliaryCalibreQuantity());
            throw new AddGunException("Exceeded the maximum number of guns of auxiliary calibre(change main calibre)!!! The maximum number of guns of auxiliary calibre is set");
        }

        setMainCalibreQuantity(w.getQuantity());
        if(getMainCalibreQuantity()>getMaxMainCalibreQuantity()){
            setMainCalibreQuantity(getMaxMainCalibreQuantity());
            throw new AddGunException("Exceeded the maximum number of guns of main calibre!!! The maximum number of guns of main calibre is set");
        }
        getGuns().add(w);
    }

}

enum Country{
    USA,
    Japan,
    USSR,
    German,
    UK,
    NONE
}