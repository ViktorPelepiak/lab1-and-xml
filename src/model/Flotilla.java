package model;

import javax.xml.bind.annotation.*;
import java.util.LinkedList;

@XmlRootElement(name = "arms")
@XmlAccessorType(XmlAccessType.FIELD)
public class Flotilla {

    @XmlElementWrapper( name = "flotilas" )
    @XmlElements({
            @XmlElement(name = "cruiser", type = Cruiser.class),
            @XmlElement(name = "destroyer", type = Destroyer.class)
    })
    private LinkedList<Ship> flotilla;

    public Flotilla() {
        this.flotilla = new LinkedList<>();
    }

    public LinkedList<Ship> getFlotilla() {
        return flotilla;
    }

    public void setFlotilla(LinkedList<Ship> flotilla) {
        this.flotilla = flotilla;
    }
}