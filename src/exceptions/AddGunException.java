package exceptions;

public class AddGunException extends RuntimeException{
    public AddGunException() {
    }

    public AddGunException(String message) {
        super(message);
    }

    public AddGunException(String message, Throwable cause) {
        super(message, cause);
    }

    public AddGunException(Throwable cause) {
        super(cause);
    }

    public AddGunException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
